<?php

/*
Disable wordpress blocks
*/

add_filter('use_block_editor_for_post_type', 'd4p_32752_disable_block_editor', 10, 2);

function d4p_32752_disable_block_editor($use_block_editor, $post_type) {
    if (in_array($post_type, array('page'))) {
        return false;
    }
    if (in_array($post_type, array('post'))) {
        return false;
    }
    return $use_block_editor;
}