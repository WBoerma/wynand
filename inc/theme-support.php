<?php

//Enable post thumbnails

add_theme_support( 'post-thumbnails' ); 

//THUMBNAIL SIZES

if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'post-thumb', 786, 500, array( 'center', 'center') );
	add_image_size( 'post-thumb-wide', 1600, 500, array( 'center', 'center') );
    add_image_size( 'post-banner', 1920, 654, true );
}