<section class="c-gallery default-spacing" data-scroll-section="Galerij">
	<div class="l-container l-container--no-space-right">
		<div class="l-content-wrap">

			<div class="c-gallery__images">
				<a class="lightbox" href="<?php echo get_template_directory_uri(); ?>/assets/images/main-banner/example3.jpg">
				<img class="c-gallery__image js-animate-in-viewport slide-in" src="<?php echo get_template_directory_uri(); ?>/assets/images/main-banner/example3.jpg" alt="" />
				</a>
				<img class="c-gallery__image c-gallery__image--small js-animate-in-viewport slide-in" src="<?php echo get_template_directory_uri(); ?>/assets/images/main-banner/example3-2.jpeg" alt="" />
				<img class="c-gallery__image c-gallery__image--small js-animate-in-viewport slide-in" src="<?php echo get_template_directory_uri(); ?>/assets/images/main-banner/example3-3.jpeg" alt="" />
			</div>

		</div>
	</div>
</section>