<section class="c-slider-products default-spacing" data-scroll-section="In de winkel">

	<div class="l-container">
		<div class="l-content-wrap l-wysiwyg js-animate-in-viewport fade-in">
			<h2>In de winkel</h2>
			<p>Draag bij aan <a href="">mijn reis</a> en een beetje aan de wereld. Voor elke bestelling plant ik 1/4e boom via Trees for All.</p>
		</div>

		<div class="c-slider-products__slider splide js-slider-products js-animate-in-viewport fade-in">
			<div class="splide__track">
				<div class="splide__list">

					<div class="splide__slide">
						
						<a href="" class="c-product-teaser">
							<div class="c-product-teaser__image">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/product/product.png" />
							</div>
							<div class="c-product-teaser__content">
								<h2 class="c-product-teaser__title">Poster Source de Lison</h2>
								<p class="c-product-teaser__note">Verschillende formaten</p>
								<p class="c-product-teaser__price">vanaf € 10</p>
							</div>
						</a>

					</div>

					<div class="splide__slide">
						
						<a href="" class="c-product-teaser">
							<div class="c-product-teaser__image">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/product/product.png" />
							</div>
							<div class="c-product-teaser__content">
								<h2 class="c-product-teaser__title">Poster Source de Lison</h2>
								<p class="c-product-teaser__note">Verschillende formaten</p>
								<p class="c-product-teaser__price">vanaf € 10</p>
							</div>
						</a>

					</div>

					<div class="splide__slide">
						
						<a href="" class="c-product-teaser">
							<div class="c-product-teaser__image">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/product/product.png" />
							</div>
							<div class="c-product-teaser__content">
								<h2 class="c-product-teaser__title">Poster Source de Lison</h2>
								<p class="c-product-teaser__note">Verschillende formaten</p>
								<p class="c-product-teaser__price">vanaf € 10</p>
							</div>
						</a>

					</div>

				</div>
			</div>
		</div>
		
	</div>

</section>