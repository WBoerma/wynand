<section class="c-map default-spacing" data-scroll-section="De locatie">

	<div class="c-map__marker c-map__marker--animation" data-lat="8.037220" data-long="98.833183" hidden/>
		<p class="c-map__marker-text">Meppel</p>
	</div>

	<div class="l-container js-animate-in-viewport fade-in">
		<div class="l-content-wrap l-wysiwyg">
		<h2>De locatie</h2>
			<div class="c-map__content">
				<p>Lorem ipsum <br /> 
				Dolor sit amet</p>
				<a class="e-button" href="/">Maps</a>
			</div>
		</div>
	</div>

	<div class="c-map__map" id="map-1" data-zoom="5"></div>

</section>