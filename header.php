<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>

		<!-- mobile meta -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta name="pinterest" content="nopin">

		<!-- FAVICON-->
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png" sizes="16x16" />
		
		<!-- STYLING -->
		<link href="<?php echo get_template_directory_uri(); ?>/assets/app.css?<?php echo date('dynHis'); ?>" rel="stylesheet">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<script src='https://api.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.js'></script>
		<link href='https://api.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.css' rel='stylesheet' />

		<?php wp_head(); ?>

	</head>

	<body <?php echo body_class(); ?>>

		<?php wp_body_open(); ?>

		<div class="app-container">

			<div class="c-menu">
				<nav class="c-menu__nav">
					<?php wp_nav_menu( array('theme_location' => 'mainnavigation', 'container' => 'c-menu__nav-list' ));?>				
				</nav>
			</div>

			<div class="c-page-menu">
				<div class="c-page-menu__head">
					<p class="c-page-menu__title">Aiguilles d'Arves</p>
					<p class="c-page-menu__watched"><span class="js-page-indicator-counter">0</span>% bekeken</p>
				</div>
				<div class="c-page-menu__progress-bar">
					<div class="c-page-menu__bar js-page-indicator-bar"></div>
				</div>
				<nav class="c-page-menu__nav">
					<ul>
					</ul>
				</nav>
			</div>

			<div class="c-page-menu-counter js-toggle-page-menu js-visible-after-scroll">
				<p class="c-page-menu-counter__count"><span class="js-page-indicator-counter">0</span>%</p>
				<div class="c-page-menu-counter__bar js-page-indicator-bar"></div>
			</div>

			<div class="l-toolbar">
				<!--
				<div class="c-page-menu-icon js-toggle-page-menu">
					<span class="c-page-menu-icon__dots"></span>
				</div>-->
			</div>

			<div class="c-menu-icon js-toggle-menu">
				<div class="c-menu-icon__bars-wrap">
					<div class="c-menu-icon__bars"></div>
				</div>
			</div>

			<header class="l-header">
				<a class="e-logo" href="<?php echo home_url(); ?>">
					<span class="e-logo__name">wynand</span>
					<span class="e-logo__note">a photography journey</span>
				</a>
			</header>

			<div class="l-page-wrap">

				<main class="l-main">
