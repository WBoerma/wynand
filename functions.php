<?php

require_once( get_template_directory() . '/inc/scripts-and-styles.php' );
require_once( get_template_directory() . '/inc/actions.php' );
require_once( get_template_directory() . '/inc/filters.php' );
require_once( get_template_directory() . '/inc/theme-support.php' );
require_once( get_template_directory() . '/inc/custom-menu-item.php' );

?>