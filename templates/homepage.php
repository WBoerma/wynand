<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>

	<?php include(get_template_directory() . '/partials/main-banner.php'); ?>

	<?php include(get_template_directory() . '/partials/intro.php'); ?>

	<?php include(get_template_directory() . '/partials/gallery.php'); ?>

	<?php include(get_template_directory() . '/partials/slider-products.php'); ?>

	<?php include(get_template_directory() . '/partials/map.php'); ?>
<?php get_footer(); ?>
