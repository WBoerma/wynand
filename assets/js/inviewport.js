//Adds animated class to an element if the element is visible in the viewport

var isInViewport = function(elem) {
  var distance = elem.getBoundingClientRect();
      offset = 50;

  //console.log('height: ' + distance.height + ' bottom: ' + distance.bottom + ' top: ' + distance.top + ' window: ' + window.innerHeight + ' client: ' + document.documentElement.clientHeight);
  
  return (
    distance.top <= (window.innerHeight || document.documentElement.clientHeight) - offset
  );
};

var animateTriggerClass = document.querySelectorAll('.js-animate-in-viewport');

function addAnimatedClass(event) {
    
  animateTriggerClass.forEach(element => {
  
    if (isInViewport(element)) {
      //if in Viewport
      element.classList.add("is-animated");
    } 
  });

}

window.addEventListener('load', addAnimatedClass);
window.addEventListener('scroll', addAnimatedClass);