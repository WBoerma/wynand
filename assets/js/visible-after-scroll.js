function visibleAfterScroll(event) {

	var offset = 100;
	var element = document.querySelectorAll('.js-visible-after-scroll');

	for ( var i = 0; i < element.length; i++ ) (function(i){ 

		if(window.pageYOffset > offset) {
			element [i].classList.add('is-visible-after-scroll');
		} else {
			element [i].classList.remove('is-visible-after-scroll');
		}

	})(i);

}

window.addEventListener('scroll', visibleAfterScroll);