import smoothscroll from 'smoothscroll-polyfill';
smoothscroll.polyfill();

var sections = document.querySelectorAll('[data-scroll-section]');
var	pageMenu = document.querySelector('.c-page-menu__nav ul');
let offset = window.innerHeight / 2;

function scrollSections(event) {
    
	sections.forEach(section => {

		var name = section.getAttribute('data-scroll-section');

		var item = document.createElement('li');
			item.setAttribute('data-section', name)
			item.innerHTML = name;

			pageMenu.appendChild(item);

			item.addEventListener('click', function(e) {

				e.preventDefault();
	            let target = document.querySelector('[data-scroll-section="' + name + '"]');
	            let elementPosition = target.offsetTop;
	            let offsetPosition = elementPosition - 20;

	            window.scroll({
				  top: offsetPosition, 
				  left: 0, 
				  behavior: 'smooth' 
				});

	            if(document.body.classList.contains('body--page-menu-is-open')) {
					document.body.classList.remove('body--page-menu-is-open');
				}

			});

			window.addEventListener('scroll', scrollSectionsPassed);


	});
}

window.addEventListener('load', scrollSections);

function scrollSectionsPassed() {

	sections.forEach(section => {

		var position = section.getBoundingClientRect();
		var name = section.getAttribute('data-scroll-section');
		var item = document.querySelector('[data-section="' + name + '"]');

		//console.log('name:' + name + ' height: ' + position.height + ' bottom: ' + position.bottom + ' top: ' + position.top + ' window: ' + window.innerHeight + ' client: ' + document.documentElement.clientHeight);
	
		if(position.top <= -position.height + offset){

			item.classList.add('passed-section');

		} else if(item.classList.contains('passed-section')) {

			item.classList.remove('passed-section');

		}

		if(position.top > -position.height + 50 && position.bottom < position.height + 50 ){
			item.classList.add('current-section');
		} else {
			item.classList.remove('current-section');
		}


	});

}

