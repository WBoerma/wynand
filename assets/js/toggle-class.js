function toggleClass(triggerClass, targetClass, className) {
	var elements = document.querySelectorAll(targetClass);
	var clickableItems = document.querySelectorAll(triggerClass);

	for ( var i = 0; i < clickableItems .length; i++ ) (function(i){ 
		clickableItems [i].onclick = function() {

			for (var e = 0; e < elements.length; e++ ) (function(e){
				elements [e].classList.toggle(className);
			})(e);
		}
	})(i);
}

//Nav bar mobile
toggleClass(".js-toggle-menu", "body", "body--menu-is-open");

toggleClass(".js-toggle-page-menu", "body", "body--page-menu-is-open");