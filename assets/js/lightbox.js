import GLightbox from 'glightbox';
import 'glightbox/dist/css/glightbox.css';

document.addEventListener('DOMContentLoaded', function(event) {

	const lightbox = GLightbox({
	    selector: '.lightbox',
	});
});