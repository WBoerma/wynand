import TinyGesture from 'tinygesture';

const options = {
  velocityThreshold: 1
};


const body = document.querySelector('body');
const gesture = new TinyGesture(body, options);

var mainMenuClass = 'body--menu-is-open';
var pageMenuClass = 'body--page-menu-is-open';

gesture.on('swipeleft', event => {

	if(body.classList.contains(pageMenuClass)) {
		body.classList.remove(pageMenuClass);
	} else {
		body.classList.add(mainMenuClass);
	}
});

gesture.on('swiperight', event => {

	if(body.classList.contains(mainMenuClass)) {
		body.classList.remove(mainMenuClass);
	} else {
		body.classList.add(pageMenuClass);
	}
});
