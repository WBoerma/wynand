mapboxgl.accessToken = 'pk.eyJ1Ijoid25kd29ya3MiLCJhIjoiY2p3YWxidTU5MDN3bTQ5bzFqNHMxNmsxZCJ9.fqYr46O61vyK3dniZwHTnw'; // replace this with your access token

//Each map
var maps = document.querySelectorAll('.c-map');

maps.forEach(mapEl => {

	var mapClass = mapEl.querySelector('.c-map__map');
	var mapID = mapClass.getAttribute('id');
	var zoom = mapClass.getAttribute('data-zoom');

	var firstMarker = mapEl.querySelector('.c-map__marker');
	var centerLat  = firstMarker.getAttribute('data-lat');
	var	centerLong  = firstMarker.getAttribute('data-long');

	var map = new mapboxgl.Map({
	  container: mapID,
	  style: 'mapbox://styles/wndworks/ckor7icla15oh18mp2v7vdlzq', // replace this with your style URL
	  center: [centerLong, centerLat ],
	  zoom: zoom,
	  zoomControl: false,
	  dragPan: false,
	  touchZoomRotate: true,
	});

	// Disable drag and zoom handlers.
	map.scrollZoom.disable();
	//map.dragging.disable();
	//map.touchZoom.disable();
	map.doubleClickZoom.disable();
	//map.scrollWheelZoom.disable();*/


	//Create mapbox marker for every marker withouth the class '.exist'

	var markers = mapEl.querySelectorAll('.c-map__marker');

	markers.forEach(element => {
	  	
	  	var classes = element.getAttribute('class');
	  		lat  = element.getAttribute('data-lat');
	    	long  = element.getAttribute('data-long');
	    	number = element.getAttribute('number');
	    	content = element.innerHTML;

	    var el = document.createElement('div');
	    	el.className = classes;

	    var popup = new mapboxgl.Popup({ offset: 0, closeButton: false, anchor: 'top'}).setMaxWidth("auto")
	    .setHTML('<div class="c-map__popup" data-lat="' + lat + '" data-long="' + long + '">' + content + '</div>');

	    new mapboxgl.Marker(el, {anchor: 'bottom'})
	    .setLngLat({lng: long, lat: lat})
	    .setPopup(popup)
	    .addTo(map);

	    //On marker click
	    el.addEventListener('click', function (e) {

	      //Fly to the right point
	      map.flyTo({
	        center: [ long, lat],
	        zoom: 12,
	        offset: [0, 0],
	        speed: 2,
	        curve: .6
	      });

	    });
	   
	  });


});