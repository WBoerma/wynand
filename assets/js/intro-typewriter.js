var introElements = document.querySelectorAll('.js-intro-typewriter');
    letterSpeed = 50;
    titleDelay = 250;
    categoryDelay = 250;
    textDelay = 250;

for ( var i = 0; i < introElements .length; i++ ) (function(i){

  var title =  introElements [i].querySelector('.c-intro__title');
      category =  introElements [i].querySelector('.c-intro__category');
      introText = introElements [i].querySelector('.c-intro__text p');

  let titleText = title.innerHTML;
      title.innerHTML= "";
      categoryText = category.innerHTML;
      category.innerHTML= "";

  var i=0;
  var iCat=0;

  setTimeout(function(){

    var timer=setInterval(function(){
        if(i<titleText.length) {
          title.append(titleText.charAt(i))
          i++;
        } else {
          clearInterval(timer);

          setTimeout(function(){

            var categoryTimer=setInterval(function(){

              if(iCat<categoryText.length) {
                category.append(categoryText.charAt(iCat))
                iCat++;
              }

              else {
                clearInterval(categoryTimer);

                setTimeout(function(){
                  introText.classList.add('animation-fade-in');
                }, textDelay );
              }

            }, letterSpeed )

          }, categoryDelay);

        }
      }, letterSpeed )

  }, titleDelay);

})(i);