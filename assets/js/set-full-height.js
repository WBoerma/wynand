//Set full height with javascript, prevents jumping causes by the the ios info bar

function setFullHeight(event) {

	var element = document.querySelectorAll('.js-full-height');

	for ( var i = 0; i < element.length; i++ ) (function(i){ 
		element [i].style.height = document.documentElement.clientHeight + 'px';
	})(i);

}

window.addEventListener('load', setFullHeight);
window.addEventListener('resize', setFullHeight);