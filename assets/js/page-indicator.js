//Page indicator

window.onscroll = function() {
  pageIndicator();
};

function pageIndicator() {

  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrollCount = (winScroll / height) * 100;
  var bar = document.querySelectorAll('.js-page-indicator-bar');
  var barCount = document.querySelectorAll('.js-page-indicator-counter');

  for ( var i = 0; i < bar.length; i++ ) (function(i){ 

    bar [i].style.width = scrollCount+ "%";
    bar [i].classList.add("is-scrolling");

  })(i);

  for ( var i = 0; i < barCount.length; i++ ) (function(i){ 

    barCount [i].innerHTML = scrollCount.toFixed(0);

  })(i);

}

// Setup isScrolling variable
var isScrolling;

window.addEventListener('scroll', function ( event ) {

  // Clear our timeout throughout the scroll
  window.clearTimeout( isScrolling );

  // Set a timeout to run after scrolling ends
  isScrolling = setTimeout(function() {

      var bar = document.querySelectorAll('.js-page-indicator-bar');

      for ( var i = 0; i < bar.length; i++ ) (function(i){ 
        bar [i].classList.remove("is-scrolling");
      })(i);

  }, 66);

}, false);