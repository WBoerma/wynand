var isInViewport = function(elem) {
  var distance = elem.getBoundingClientRect();
  return (
    distance.top >= 0 &&
    distance.left >= 0 &&
    distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    distance.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

function typingEffect(element,speed,){

  let text = element.innerHTML;
      element.innerHTML="";

  var i=0;

  var timer=setInterval(function(){
    if(i<text.length) {
      element.append(text.charAt(i))
      i++;
    } else {
      clearInterval(timer);
    }
  }, speed)
  
}

function playTypingEffect(event) {

  var textItems = document.querySelectorAll('.js-typewriter');

  for ( var i = 0; i < textItems .length; i++ ) (function(i){

        var textItem = textItems [i];

        if (isInViewport(textItem)) {
          typingEffect(textItem,50);
        }

  })(i);

}

window.addEventListener('scroll', playTypingEffect);





