import Splide from '@splidejs/splide';

document.addEventListener( 'DOMContentLoaded', function () {

	var splide = new Splide( '.js-slider-products', {
		perPage: 2,
		gap: '2rem',
		autoWidth: true,
		padding: {
			left : '4rem',
			right: '4rem',
		},
		arrows: false,
		breakpoints: {
			1024: {
				perPage: 2,
				autoWidth: true,
				padding: {
					left : '6rem',
					right: '0',
				},
			},
			480: {
				perPage: 1,
				autoWidth: true,
				padding: {
					left : '6rem',
					right: '6rem',
				},
			},
		}
	} ).mount();

});